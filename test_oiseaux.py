import oiseaux
# --------------------------------------
# FONCTIONS
# --------------------------------------

def test_recherche_oiseau():
    assert oiseaux.recherche_oiseau(oiseaux.oiseaux,"Mésange")==("Mésange","Passereau")
    assert oiseaux.recherche_oiseau(oiseaux.oiseaux,"Corbeau")==None
test_recherche_oiseau()

def test_recherche_par_famille():
    assert oiseaux.recherche_par_famille(oiseaux.oiseaux,"Passereau")==["Mésange","Moineau","Pinson","Rouge-gorge"]
    assert oiseaux.recherche_par_famille(oiseaux.oiseaux,"Corbeau")==[]
test_recherche_par_famille()

def test_oiseau_le_plus_observe():
    assert oiseaux.oiseau_le_plus_observe(oiseaux.observations1)=="Moineau"
    assert oiseaux.oiseau_le_plus_observe(oiseaux.observations2)=="Tourterelle"
    assert oiseaux.oiseau_le_plus_observe(oiseaux.observations3)=="Mésange"
    assert oiseaux.oiseau_le_plus_observe([])==None
test_oiseau_le_plus_observe()

def test_est_liste_observations():
    assert oiseaux.est_liste_observations(oiseaux.observations1)==False
    assert oiseaux.est_liste_observations(oiseaux.observations4)==True
    assert oiseaux.est_liste_observations(oiseaux.observations5)==False
    assert oiseaux.est_liste_observations(oiseaux.observations6)==False
test_est_liste_observations()

def test_max_observations():
    assert oiseaux.max_observations(oiseaux.observations1)==5
    assert oiseaux.max_observations(oiseaux.observations2)==5
    assert oiseaux.max_observations(oiseaux.observations3)==4
    assert oiseaux.max_observations([])==0
test_max_observations()

def test_moyenne_oiseaux_observes():
    assert oiseaux.moyenne_oiseaux_observes(oiseaux.observations1)==3.0
    assert oiseaux.moyenne_oiseaux_observes(oiseaux.observations2)==2.5
    assert oiseaux.moyenne_oiseaux_observes(oiseaux.observations3)==2.5
test_moyenne_oiseaux_observes()
def test_total_famille():
    assert oiseaux.total_famille(oiseaux.oiseaux,oiseaux.observations1,"Passereau")==8
    assert oiseaux.total_famille(oiseaux.oiseaux,oiseaux.observations2,"Turtidé")==2
     assert oiseaux.total_famille(oiseaux.oiseaux,oiseaux.observations3,"Colombidé")==4
test_total_famille()


def test_construire_liste_observations():
    assert oiseaux.construire_liste_observations(oiseaux.oiseaux,oiseaux.comptage1)==['Merle', 2, 'Mésange', 0, 'Moineau', 5, 'Pic vert', 1, 'Pie', 2, 'Pinson', 0, 'Tourterelle', 5, 'Rouge-gorge', 3]
    assert oiseaux.construire_liste_observations(oiseaux.oiseaux,oiseaux.comptage1)==['Merle', 2, 'Mésange', 1, 'Moineau', 3, 'Pic vert', 0, 'Pie', 0, 'Pinson', 3, 'Tourterelle', 5, 'Rouge-gorge', 1]
    assert oiseaux.construire_liste_observations(oiseaux.oiseaux,oiseaux.comptage3)==['Merle', 0, 'Mésange', 4, 'Moineau', 0, 'Pic vert', 3, 'Pie', 2, 'Pinson', 1, 'Tourterelle', 4, 'Rouge-gorge', 2]

def test_-
:
    assert oiseaux.creer_ligne_sup(...)==...

def test_creer_ligne_noms_oiseaux():
    assert oiseaux.creer_ligne_noms_oiseaux(...)==...
