#Exercice1
def plus_long_plateau(chaine):
    """recherche la longueur du plus grand plateau d'une chaine

    Args:
        chaine (str): une chaine de caractères

    Returns:
        int: la longueur de la plus grande suite de lettres consécutives égales
    """    
    lg_max=0 # longueur du plus grand plateau déjà trouvé
    lg_actuelle=0 #longueur du plateau actuel
    for i in range(len(chaine)):
        if chaine[i]==chaine[i-1]: # si la lettre actuelle est égale à la précédente
            lg_actuelle+=1
        else: # si la lettre actuelle est différente de la précédente
            if lg_actuelle>lg_max:
                lg_max=lg_actuelle
            lg_actuelle=1
    if lg_actuelle>lg_max: #cas du dernier plateau
        lg_max=lg_actuelle
    return lg_max

def test_plus_long_plateau():
    assert plus_long_plateau("kkaaayyyyaaakk")==4
    assert plus_long_plateau("bbbbbboonnjjoouurrrr")==6
    assert plus_long_plateau("chhevvaaallllllllll")==10
    assert plus_long_plateau("cchat")==2
    
test_plus_long_plateau()

#Exercice 2
def villes(liste_villes,population):
    res=None
    max=0
    for i in range(len(population)):
        if population[i]>max:
            max=population[i]
            res=liste_villes[i]
    return res

liste_villes=["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"]
population=[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]

def test_villes():
    assert villes(liste_villes,population)=="Tours"

test_villes()

#Exercice 3
def transformation(chaine):
    """Fonction qui transforme une chaine de caractère en nombre

    Args:
        chaine (str): chaine de caractère à transformer

    Returns:
        int: chaine de caractère transformée
    """
    res=0
    for i in range(len(chaine)):
        if chaine[i]=='0':
            res=res*10+0
        elif chaine[i]=='1':
            res=res*10+1
        elif chaine[i]=='2':
            res=res*10+2
        elif chaine[i]=='3':
            res=res*10+3
        elif chaine[i]=='4':
            res=res*10+4
        elif chaine[i]=='5':
            res=res*10+5
        elif chaine[i]=='6':
            res=res*10+6
        elif chaine[i]=='7':
            res=res*10+7
        elif chaine[i]=='8':
            res=res*10+8
        elif chaine[i]=='9':
            res=res*10+9
    return res

def test_transformation():
    assert transformation('2021')==2021
    assert transformation('211103')==211103
    assert transformation('456789')==456789

test_transformation()

#Exercice 4
def recherche_mots(liste,lettre):
    """Fonction qui permet de retrouver les mots qui commencent par une certaine lettre dans
une liste de mots

    Args:
        liste (str): liste de mots
        lettre (str): la lettre que doit commencer le mot

    Returns:
        str: liste de mots commençant par la même lettre
    """  
    res=[]
    for mot in liste:
        if mot[0]==lettre:
            res.append(mot)   
    return res

def test_recherche_mots():
    assert recherche_mots(['salut','hello','hallo','ciao','hola'],'h')==['hello','hallo','hola']
test_recherche_mots

#Exercice 5
def decoupage_mots(phrase):
    res=""
    liste=[]
    for ma_var in phrase:
        if (ma_var.isalpha()==True):
            res=res+ma_var
        elif res!="":
            liste.append(res)
            res=""
    return liste
def test_decoupage_mots():
    assert decoupage_mots("Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool!!")==['Cela', 'fait', 'déjà', 'jours', 'jours', 'à', 'l', 'IUT', 'O', 'Cool']
    assert decoupage_mots("(3*2)+1")==[]
test_decoupage_mots()

#Exercice 6
def recherche_mots_bis(phrase,lettre):
    liste=decoupage_mots(phrase)
    res=recherche_mots(liste,lettre)
    return res

def test_recherche_mots_bis():
    assert recherche_mots_bis("Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool!!",'C')==['Cela', 'Cool']

#Exercice 7
def liste(N):
    res=[True]*(N+1)
    res[0]=False
    res[1]=False
    return res

def multiple(x,list):
    for i in range(2*x,len(list),x):
        list[i]=False
    return list
maliste=liste(17)   
print(multiple(3,maliste))

def crible(N):
    res=[]
    list=liste(N)
    for i in range(2,len(list)):
        if list[i]:
            res.append(i)
            multiple(i,list)
    return res
print(crible(17))
