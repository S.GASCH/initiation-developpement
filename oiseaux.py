# --------------------------------------
# DONNEES
# --------------------------------------

# exemple de liste d'oiseaux observables
oiseaux=[
        ("Merle","Turtidé"), ("Mésange","Passereau"), ("Moineau","Passereau"), 
        ("Pic vert","Picidae"), ("Pie","Corvidé"), ("Pinson","Passereau"),
        ("Tourterelle","Colombidé"), ("Rouge-gorge","Passereau")
        ]
# exemples de listes de comptage ces listes ont la même longueur que oiseaux
comptage1=[2,0,5,1,2,0,5,3]
comptage2=[2,1,3,0,0,3,5,1]
comptage3=[0,4,0,3,2,1,4,2]

# exemples de listes d'observations. Notez que chaque liste correspond à la liste de comptage de
# même numéro
observations1=[
        ("Merle",2),  ("Moineau",5), ("Pic vert",1), ("Pie",2), 
        ("Tourterelle",5), ("Rouge-gorge",3)
            ]

observations2=[
        ("Merle",2), ("Mésange",1), ("Moineau",3), 
        ("Pinson",3),("Tourterelle",5), ("Rouge-gorge",1)
            ]

observations3=[
        ("Mésange",4),("Pic vert",2), ("Pie",2), ("Pinson",1),
        ("Tourterelle",4), ("Rouge-gorge",2)
            ]
observations4=[
        ("Mésange",4),("Pic vert",2), ("Pie",2), ("Pinson",1),
        ("Rouge-gorge",2),("Tourterelle",4)
            ]
observations5=[
        ("Mésange",4),("Pic vert",2), ("Pie",2), ("Pinson",1),
        ("Rouge-gorge",2),("Tourterelle",0)
            ]
observations6=[
        ("Tourterelle",0),("Mésange",4),("Pic vert",2), ("Pie",2), 
        ("Pinson",1),("Rouge-gorge",2)
            ]
# --------------------------------------
# FONCTIONS
# --------------------------------------
# Exercice 1
def oiseau_le_plus_observe(liste_observations):
    """ recherche le nom de l'oiseau le plus observé de la liste (si il y en a plusieur on donne le 1er trouve)

    Args:
        liste_observations (list): une liste de tuples (nom_oiseau,nb_observes)

    Returns:
        str: l'oiseau le plus observé (None si la liste est vide)
    """
    oiseau_max=0
    nom_oiseau=None
    for i in range(len(liste_observations)):
        if liste_observations[i][1]>oiseau_max:
            oiseau_max=liste_observations[i][1]
            nom_oiseau=liste_observations[i][0]
    return nom_oiseau

# Exercice 2
def recherche_oiseau(liste_oiseaux,nom_oiseau):
    """Recherche un oiseau à partir de son nom

    Args:
        oiseaux (list): une liste d'oiseaux
        nom_oiseau (str): le nom de l'oiseau recherchés
    Returns:
        tuple: le tuple représentant l'oiseau recherché (None si aucun oiseau ne correspond)
    """    
    for i in range(len(liste_oiseaux)):
        if nom_oiseau==liste_oiseaux[i][0]:
            return liste_oiseaux[i]
    return None

def recherche_par_famille(liste_oiseaux,famille_oiseau):
    res=[]
    for i in range(len(liste_oiseaux)):
        if famille_oiseau==liste_oiseaux[i][1]:
            res.append(liste_oiseaux[i][0])
    return res

# Exercice 3
def est_liste_observations(liste_observations):
    for i in range(1,len(liste_observations)):
        if liste_observations[i][1]==0:
            return False
        elif liste_observations[i-1][0]>=liste_observations[i][0]:
            return False
    return True

def max_observations(liste_observations):
    res=0
    for i in range (len(liste_observations)):
        if liste_observations[i][1]>res:
            res=liste_observations[i][1]
    return res

def moyenne_oiseaux_observes(liste_observations):
    res=0
    cpt=0
    for i in range(len(liste_observations)):
        res+=liste_observations[i][1]
        cpt+=1
    return res/cpt

def total_famille(liste_oiseaux,liste_observations,famille_oiseau):
    res=0
    selection=recherche_par_famille(liste_oiseaux,famille_oiseau)
    for i in range(len(liste_observations)):
        if liste_observations[i][0] in selection:
            res=res+liste_observations[i][1]
    return res

# Exercice 4
def construire_liste_observations(liste_oiseaux,liste_comptages):
    res=[]
    for i in range(len(liste_oiseaux)):
        if liste_comptages!=None:
            res.append(liste_oiseaux[i][0])
            res.append(liste_comptages[i])
    return res

def saisie_observations(liste_oiseaux):
    liste_rep=[]
    for i in range(len(liste_oiseaux)):
        print("Entrez les observations de",liste_oiseaux[i][0],"de la famille",liste_oiseaux[i][1])
        rep=int(input())
        liste_rep.append(rep)
    return construire_liste_observations(liste_oiseaux,liste_rep)
print(saisie_observations(oiseaux))

# Exercice 5
def afficher_observations(liste_observations,liste_oiseaux):
    for observation in liste_observations:
        oiseau=recherche_oiseau(liste_oiseaux,observation[0])
        print("Nom",oiseau[0].ljust(20),"Famille",oiseau[1].ljust(20),"Nbre obsvervés",observation[1])

def creer_ligne_sup(liste_observations,seuil):
    

    


#--------------------------------------
# PROGRAMME PRINCIPAL
#--------------------------------------

#afficher_graphique_observation(construire_liste_observations(oiseaux,comptage3))
#comptage=saisie_observations(oiseaux)
#afficher_graphique_observation(comptage)
#afficher_observations(comptage,oiseaux)
