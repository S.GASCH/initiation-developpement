#Exercice 1
def mystere(liste,valeur):
    """Fonction qui 

    Args:
        liste (list): liste de nombres
        valeur (int): valeur initiale

    Returns:
        [int]: Soit la valeur de x ou None
    """
    xxx=0
    yyy=0
    for i in range(len(liste)):
        if liste[i]==valeur:
            yyy+=1
            if yyy>3:
                return xxx
        xxx+=1
    return None
print(mystere([12,5,8,48,12,418,185,17,5,87],20))

#Exercice 2
def recherche_information(phrase):
    res=None
    for i in range(len(phrase)):
        if phrase[i]==0:
            res=0
            return res
        elif phrase[i]==1:
            res=1
            return res
        elif phrase[i]==2:
            res=2
            return res
        elif phrase[i]==3:
            res=3
            return res
        elif phrase[i]==4:
            res=4
            return res
        elif phrase[i]==5:
            res=5
            return res
        elif phrase[i]==6:
            res=6
            return res
        elif phrase[i]==7:
            res=7
            return res
        elif phrase[i]==8:
            res=8
            return res
        elif phrase[i]==9:
            res=9
            return res
        else:
            return None
print(recherche_information("on est le 30/09/2021"))

def villes(liste_villes,population,ville):
    for i in range(len(liste_villes)):
        if liste_villes[i]==ville:
            return population[i]
    return None
liste_villes=["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"]
population=[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]
print(villes(liste_villes,population,"Paris"))

#Exercice 3
def ordre_croissant(liste):
    for i in range(1,len(liste)):
        if liste[i-1]>liste[i]:
            return False
    return True
print(ordre_croissant([1,2,8,4,5,6]))

def somme(liste,seuil):
    cpt=0
    for i in range(len(liste)):
        cpt+=liste[i]
        if cpt>seuil:
            return True
    return False
print(somme([1,1,1],6))

def email(mot):
    arobase=False
    point=False
    if mot[0]=='@' and mot[-1]=='.':
        return False
    for i in range(len(mot)):
        if mot[i]==' ':
            return False
        elif mot[i]=='@' and arobase==False:
            arobase=True
        elif mot[i]=='@' and arobase==True:
            return False
        elif mot[i]=='.' and arobase==True:
            if point==False:
                point=True
            elif point==True:
                return False
    return arobase and point 
print(email('sgaschig581@gmail.com'))

#Exercice 4
def meilleur_score(scores,joueurs,nom):
    for i in range(len(joueurs)):
        if joueurs[i]==nom:
            return scores[i]
    return None
scores=[352100,325410,312785,220199,127853]
joueurs=['Batman','Robin','Batman','Joker','Batman']
print(meilleur_score(scores,joueurs,'Batman'))

def ordre_decroissant(scores):
    for i in range(1,len(scores)):
        if scores[i-1] < scores[i]:
            return False
    return True
print(ordre_decroissant(scores))

def classement(joueurs,pseudo):
    cpt=0
    for i in range(len(joueurs)):
        if joueurs[i]==pseudo:
            cpt+=1
    return cpt
print(classement(joueurs,'Paris'))

def insertion(scores,nouv_scores):
    for i in range(len(scores)):
        if scores[i] < nouv_scores:
            return i
    return None
print(insertion(scores,314570))

def insertionbis(score,joueur,scores,joueurs):
    res=0
    res=insertion(scores,nouv_scores)
    if res is None:
        joueurs.append(joueur)
        scores.append(score)
    else:
        joueurs.insert(res,joueur)
        scores.insert(res,liste_score)
    return scores,joueurs
print(insertionbis(129000,'Robin',scores,joueurs))
