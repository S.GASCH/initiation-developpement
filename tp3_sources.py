# exercice 2

def mystere(liste_nbre):
    """[summary]

    Args:
        entree ([type]): [description]

    Returns:
        [type]: [description]
    """
    xxx=0
    yyy=0
    # au début de chaque tour de boucle
    # xxx contient le nombre de chiffres pairs 
    # yyy contient le nombre de chiffre impairs
    for zzz in liste_nbre:
        if zzz%2==0:
            xxx+=1
        else:
            yyy+=1
    return xxx>=yyy

def test_mystere():
    assert mystere([1,4,6,-2,-5,3,10])==True
    assert mystere([-4,5,-11,-56,5,-11])==False
    assert mystere([-2,2,-13,-56,3,-9,14])==True
    assert mystere([-7,3,-10,-52,9,-1])==False

test_mystere()

# exercice 3

def min_sup(liste_nombres,valeur):
    """trouve le plus petit nombre d'une liste supérieur à une certaine valeur

    Args:
        liste_nombres (list): la liste de nombres
        valeur (int ou float): la valeur limite du minimum recherché

    Returns:
        int ou float: le plus petit nombre de la liste supérieur à valeur
    """
    res=None
    # au début de chaque tour de boucle res est le plus petit élément déjà énuméré
    # supérieur à valeur
    for elem in liste_nombres:
        if res==None:
            if elem>valeur:
                res=elem
        elif elem>valeur and elem<res:
            res=elem
    return res



def test_min_sup():
    assert min_sup([8,12,7,3,9,2,1,4,9],5)==7
    assert min_sup([-2,-5,2,9.8,-8.1,7],0)==2
    assert min_sup([5,7,6,5,7,3],10)==None
    assert min_sup([],5)==None

test_min_sup()

# exercice 4
def nb_mots ( phrase ) :
    """Fonction qui compte le nombre de mots d'une phrase

    Args:
        phrase (str): une phrase dont les mots sont séparés par des espaces (éventuellement plusieurs)

    Returns:
        int: le nombre de mots de la phrase
    """    
    resultat =0
    c1 = ' '
    # au début de chaque tour de boucle
    # c1 vaut c2
    # c2 vaut c1
    # resultat vaut 0
    for c2 in phrase :
        if c1 == ' ' and c2 != ' ':
            resultat = resultat +1 
        c1 = c2
    return resultat


def test_nb_mots():
    assert nb_mots("bonjour, il fait beau")==4
    assert nb_mots("houla!     je    mets beaucoup   d'  espaces    ")==6
    assert nb_mots("ce  test ne  marche pas")==5
    assert nb_mots("")==0 #celui ci non plus

test_nb_mots()


# exercice 5
def somme_nbre_paire(liste):
    """Fonction qui additionne les nombres pairs dans une liste

    Args:
        liste (int): une liste avec plusieurs entiers

    Returns:
        int: la somme des nombres pairs
    """ 
    res=0
    for elem in liste:
        if elem%2==0:
            res=res+elem
    return res

def test_somme_nbre_paire():
    assert somme_nbre_paire([12,13,6,5,7])==18
    assert somme_nbre_paire([15,14,7,6,7])==20
    assert somme_nbre_paire([10,13,1,5,7])==10
    assert somme_nbre_paire([6,6,6,6,6])==30

test_somme_nbre_paire()

def voyelle(phrase):
    """Fonction qui retourne la dernière voyelle d’une phrase

    Args:
        phrase (str): une phrase avec plusieurs voyelles

    Returns:
        str: la dernière voyelle
    """ 
    res=None
    voyelle='aeiouy'
    for lettre in phrase:
        if lettre in voyelle:
            res=lettre
    return res

def test_voyelle():
    assert voyelle("buongiorno")=="o"
    assert voyelle("bonjour")=="u"
    assert voyelle("zrt")==None
    assert voyelle("etape")=="e"

test_voyelle()

def nbre_negatif(liste):
    """Fonction qui donne la proportion de nombres strictement négatifs dans une liste

    Args:
        list (int): une liste avec des entiers

    Returns:
        float: la proportion de nombres strictement négatifs dans une liste
    """ 
    res=0
    for elem in liste:
        if elem<0:
            res=res+1
    if len(liste)==0:
       return 0
    else:
        return res/len(liste)

def test_nbre_negatif():
    assert nbre_negatif([4,-2,8,2,-2,-7])==0.5
    assert nbre_negatif([-4,-2,-8,-2,-2,-7])==1
    assert nbre_negatif([4,2,8,2,2,7])==0
    assert nbre_negatif([])==0

test_nbre_negatif()

#exercice 6
def somme(n):
    """Fonction qui fait la somme des n premiers entiers

    Args:
        n(int): un entier

    Returns:
        int: la somme des n premiers entiers
    """ 
    res=0
    for i in range(n+1):
        res=res+i
    return res

def test_somme():
    assert somme(4)==10
    assert somme(5)==15
    assert somme(6)==21
    assert somme(7)==28
test_somme()

def syracuse(U0,n):
    """Fonction qui permet de calculer le terme Un d’une suite de Syracuse

    Args:
        U0 (int): la valeure initiale
        n (int): un entier

    Returns:
        int: Valeur Un
    """
    res=U0
    for i in range(n):
        if res%2==0:
            res=res/2
        else:
            res=3*res+1
    return res

def test_syracuse():
    assert syracuse(15,9)==40
    assert syracuse(6,3)==5
    assert syracuse(14,17)==1
test_syracuse()
