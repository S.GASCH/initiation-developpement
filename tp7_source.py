"""TP7 une application complète
ATTENTION VOUS DEVEZ METTRE DES DOCSTRING A TOUTES VOS FONCTIONS"""
def afficher_menu(titre,liste_options):
    """Affiche un menu

    Args:
        titre (str): Titre 
        liste_options (list): Liste d'options
    """
    print("+-----------------------+")
    print("|"+titre+"|")
    print("+-----------------------+")
    for i in range(len(liste_options)):
        print(str(i+1)+"-> "+liste_options[i])

def demander_nombre(message,borne_max):
    """Afficher à l’utilisateur le message et attend sa réponse.

    Args:
        message (str): Un message d’invite
        borne_max (int): Un nombre entier compris entre 1 et lui-même
    """   
    
    print(message)
    rep=input()
    if int(rep)<=borne_max and int(rep)>0 and rep.isdecimal():
        return int(rep)
    else:
        return None


def menu(titre,liste_options):
    """Afficher le menu et demande à l’utilisateur sa réponse

    Args:
        titre (str): Titre
        liste_options (list): Liste d'options
    """    
    afficher_menu(titre,liste_options)
    return(demander_nombre("Entrez votre choix [1-"+str(len(liste_options))+"]",len(liste_options)))

def programme_principal():
    """Programme Principal

    Args:
        liste_options (list): Liste d'options
    """    
    liste_options=["Charger un fichier","Rechercher la population d'une commune","Afficher la population d'un département","Quitter"]
    liste_communes=[]
    while True:
        rep=menu("MENU DE MON APPLICATION",liste_options)
        if rep is None:
            print("Cette option n'existe pas")
        elif rep==1:
            print("Vous avez choisi",liste_options[rep-1])
        elif rep==2:
            print("Vous avez choisi",liste_options[rep-1])
        elif rep==3:
            print("Vous avez choisi",liste_options[rep-1])
        elif rep==4:
            print("Vous avez choisi",liste_options[rep-1])
        else:
            break
        input("Appuyer sur Entrée pour continuer")
    print("Merci au revoir!")




def charger_fichier_population(nom_fic):
    ...

def population_d_une_commune(liste_pop,nom_commune):
    ...

def liste_des_communes_commencant_par(liste_pop,debut_nom):
    ...

def commune_plus_peuplee_departement(liste_pop, num_dpt):
    ...

def nombre_de_communes_tranche_pop(liste_pop,pop_min,pop_max):
    ...

def place_top(commune, liste_pop):
    ...

def ajouter_trier(commune,liste_pop,taille_max):
    ...
    

def top_n_population(liste_pop,nb):
    ...

def population_par_departement(liste_pop):
    ...

def sauve_population_dpt(nom_fic,liste_pop_dep):
    ...

# appel au programme principal
programme_principal()
